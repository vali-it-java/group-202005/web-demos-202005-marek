
function operationAdd() {
    const number1 = parseInt(document.querySelector('#number1-input > input').value);
    const number2 = parseInt(document.querySelector('#number2-input > input').value);
    const result = number1 + number2;
    document.querySelector('#result').textContent = result;
}

function operationSub() {
    const number1 = parseInt(document.querySelector('#number1-input > input').value);
    const number2 = parseInt(document.querySelector('#number2-input > input').value);
    const result = number1 - number2;
    document.querySelector('#result').textContent = result;
}

function operationMult() {
    const number1 = parseInt(document.querySelector('#number1-input > input').value);
    const number2 = parseInt(document.querySelector('#number2-input > input').value);
    const result = number1 * number2;
    document.querySelector('#result').textContent = result;
}

function operationDiv() {
    const number1 = parseInt(document.querySelector('#number1-input > input').value);
    const number2 = parseInt(document.querySelector('#number2-input > input').value);
    const result = (number1 / number2).toFixed(5);
    document.querySelector('#result').textContent = result;
}

function operationClear() {
    document.querySelector('#number1-input > input').value = '';
    document.querySelector('#number2-input > input').value = '';
    document.querySelector('#result').textContent = 'RESULT';
}